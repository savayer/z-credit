'use strict';

$(function () {
    $('.clients').slick({
        infinite: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 5,
        slidesToScroll: 5,
        arrows: true,
        prevArrow: '<i class="clients-arrow clients-prev-arrow"><img src="img/slide-left.png"></i>',
        nextArrow: '<i class="clients-arrow clients-next-arrow"><img src="img/slide-right.png"</i>',
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4,
                infinite: true,
                arrows: true,
                prevArrow: '<i class="clients-arrow clients-prev-arrow"><img src="img/slide-left.png"></i>',
                nextArrow: '<i class="clients-arrow clients-next-arrow"><img src="img/slide-right.png"</i>'
            }
        }, {
            breakpoint: 890,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                arrows: true,
                prevArrow: '<i class="clients-arrow clients-prev-arrow"><img src="img/slide-left.png"></i>',
                nextArrow: '<i class="clients-arrow clients-next-arrow"><img src="img/slide-right.png"</i>'
            }
        }, {
            breakpoint: 601,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                arrows: true,
                prevArrow: '<i class="material-icons clients-arrow clients-prev-arrow">arrow_back_ios</i>',
                nextArrow: '<i class="material-icons clients-arrow clients-next-arrow">arrow_forward_ios</i>'
            }
        }, {
            breakpoint: 351,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                arrows: true,
                prevArrow: '<i class="material-icons clients-arrow clients-prev-arrow">arrow_back_ios</i>',
                nextArrow: '<i class="material-icons clients-arrow clients-next-arrow">arrow_forward_ios</i>'
            }
        }]
    });

    $('.navigation__join').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({ scrollTop: $('#contact_form').offset().top - 100 }, 800);
    });

    $('#toggle_menu').on('click', function () {
        $(this).toggleClass('change');
        $('#toggle_menu2').toggleClass('change');
        $('.menu-mobile-container').fadeIn();
    });

    $('#toggle_menu2').on('click', function () {
        $(this).toggleClass('change');
        $('#toggle_menu').toggleClass('change');
        $('.menu-mobile-container').fadeOut();
    });

    $('.menu-mobile .menu__li.dropdown').on('click', function () {
        $(this).children('.dropdown-menu').slideToggle();
        $(this).children('.dropdown').toggleClass('expand');

        $(this).children('.dropdown').hasClass('expand') ? $(this).children('.material-icons').text('expand_less') : $(this).children('.material-icons').text('expand_more');
    });

    if ($(window).width() >= 890) {
        $('.menu__li.dropdown').hover(function () {
            var _this = this;

            setTimeout(function () {
                $(_this).addClass('hover');
            }, 300);
        }, function () {
            var _this2 = this;

            setTimeout(function () {
                $(_this2).removeClass('hover');
            }, 400);
        });
    }

    if ($(window).width() <= 600) {
        var decisionItems = document.querySelectorAll('.decisions__item');
        $('.decision__image').removeAttr('data-aos');
        $('.decision__content').removeAttr('data-aos');
        decisionItems.forEach(function (item, index) {
            if ((index + 1) % 2 === 0) {
                item.setAttribute('data-aos', 'fade-left-mini');
            } else {
                item.setAttribute('data-aos', 'fade-right-mini');
            }
        });

        var reviews = document.querySelectorAll('.reviews__review');
        $('.review__image').removeAttr('data-aos');
        $('.review__text').removeAttr('data-aos');
        reviews.forEach(function (item, index) {
            if ((index + 1) % 2 === 0) {
                item.setAttribute('data-aos', 'fade-left-medium');
            } else {
                item.setAttribute('data-aos', 'fade-right-medium');
            }
        });

        $('.benefits__item').attr('data-aos-offset', '0');
    }
    AOS.init({
        once: true,
        duration: 500
    });
});